const db = require('../database/connect');
const filtrarResposta = require('../utils/filter/filter.service');

// Função para verificar se os dados do usuário são válidos
const verifyUser = async ({ name, email, password }) => {
    try {
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        const isEmailValid = emailRegex.test(email); // Validação do formato do email
        const isUserValid = name.length >= 4 && password.length >= 6; // Validação do tamanho do nome e da senha


        // Verificar se todos os campos são preenchidos corretamente
        if (!name || !email || !password) {
            return { status: 400, message: 'Todos os campos devem ser preenchidos' }; // Campos obrigatórios ausentes
        } else if (!isEmailValid) {
            return { status: 400, message: 'Email inválido' }; // Formato de email inválido
        } else if (!isUserValid) {
            return { status: 400, message: 'Nome ou senha inválido' }; // Nome ou senha não atendem aos critérios mínimos
        } else {
            return { status: 200, message: 'Usuário válido' }; // Dados válidos
        }
    } catch (error) {
        console.error(error); // Log de erro para depuração
        return { status: 500, message: 'Erro no servidor' }; // Erro interno do servidor
    }
};

// Função para buscar todos os usuários
const getUsers = async (req, res) => {
    const query = await db.query(
        'SELECT * FROM Usuario'
    )

    const queryFiltered = filtrarResposta(query); // Filtra a resposta da query

    try {
        return queryFiltered; // Retorna a resposta filtrada
    } catch (error) {
        console.error(error); // Log de erro para depuração
    }
}

// Função para buscar um usuário pelo ID
const getById = async (idUsuario) => {
    const query = await db.query(
        'SELECT * FROM usuario WHERE idUsuario = ?',
        [idUsuario]
    );
    const queryFiltered = filtrarResposta(query); // Filtra a resposta da query

    try {
        return queryFiltered.length > 0 ? queryFiltered[0] : null; // Retorna o usuário se encontrado
    } catch (error) {
        console.error(error); // Log de erro para depuração
        throw new error('Usuário não encontrado'); // // Lança erro se o usuário não for encontrado
    }
}

module.exports = {
    getUsers,
    getById,
    verifyUser,
};
