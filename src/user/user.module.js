const router = require('express').Router();
const userController = require('./user.controller')

router.post('/createUser', userController.createUser);
router.get('/getAllUsers', userController.getAllUsers);
router.get('/getUserById/:idUsuario', userController.getUserById);
router.put('/updateUser/:idUsuario', userController.updateUser);
router.delete('/deleteUser/:idUsuario', userController.deleteUser);

module.exports = router