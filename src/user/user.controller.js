const userService = require('./user.service');
const db = require('../database/connect');
const bcrypt = require('bcrypt');

class UserController {

    // Método para cadastrar um novo usuário
    static async createUser(req, res) {
        const { name, email, password } = req.body;
        const passwordHashed = bcrypt.hashSync(password, 10); // Hash da senha com bcrypt

        try {
            // Chama o serviço de validação
            const validationResult = await userService.verifyUser(req.body);

            if (validationResult.status === 200) { // Validação bem-sucedida
                const query = await db.query(
                    'INSERT INTO Usuario (name, email, password) VALUES (?, ?, ?)',
                    [name, email, passwordHashed]
                );
                return res.status(201).json({
                    message: "Usuário cadastrado com sucesso",
                    userId: query.insertId // // Retorna o ID do usuário cadastrado
                });
            } else {
                // Se a validação falhou, retorna o erro específico
                return res.status(validationResult.status).json({
                    error: validationResult.message
                });
            }
        } catch (error) {
            console.error(error); // Log do erro para depuração
            return res.status(500).json({ error: 'Erro interno do servidor' });
        }
    }

    // Método para buscar todos os usuários
    static async getAllUsers(req, res) {
        // Chama o serviço que busca todos os usuários
        const get = await userService.getUsers();

        try {
            // Retorna a lista de usuários
            return res.status(200).json({
                message: "Estas são os usuários cadastrados:", get
            });
        } catch (error) {
            console.error(error); // Log de erro para depuração
            return res.status(500).json({
                error: "Não foi possível conectar à base de dados SQL.",
                details: error.message,
            });
        }
    }

    // Método para buscar um usuário pelo ID
    static async getUserById(req, res) {
        const idUsuario = req.params.idUsuario;

        try {
            // Chama o serviço que busca o usuário pelo ID via parametros
            const user = await userService.getById(idUsuario);

            if (!user) {
                return res.status(404).json({ message: 'Usuário não encontrado' })
            } // Retorna um erro caso o usuário não seja encontrado

            return res.status(200).json({
                message: `Este é o usuário cadastrado do id: ${idUsuario}`,
                user,
            }); // Retorna o usuário cadastrado
        } catch (error) {
            console.error(error); // Log de erro para depuração
            return res.status(500).json({
                error: "Não foi possível conectar à base de dados SQL.",
                details: error.message,
            });
        }
    }

    // Método para atualizar os dados de um usuário
    static async updateUser(req, res) {
        const { name, email, password } = req.body;
        const { idUsuario } = req.params;

        try {
            // Verifica quais campos estão presentes na requisição para atualizar
            const fields = {};
            if (name) fields.name = name;
            if (email) fields.email = email;
            if (password) fields.password = bcrypt.hashSync(password, 10); // Hash da nova senha

            if (Object.keys(fields).length === 0) {
                return res.status(400).json({ message: 'Nenhum campo para atualizar' });
            }

            // Constrói a query dinamicamente com os campos a serem atualizados
            const setClause = Object.keys(fields)
                .map(field => `${field} = ?`) 
                .join(', ');
            const values = Object.values(fields);

            // Adiciona o idUsuario ao final dos valores para a query
            values.push(idUsuario);

            const query = await db.query(
                `UPDATE usuario SET ${setClause} WHERE idUsuario = ?`,
                values
            ); // Query para atualizar os dados do usuário

            return res.status(200).json({
                message: "Usuário atualizado com sucesso",
                userId: idUsuario
            }); // Retorna status 200 caso os dados do usuário sejam atualizados
        } catch (e) {
            console.error(e); // Log de erro para depuração
            return res.status(500).json({ error: "Erro ao atualizar o usuário" });
        }
    }

    // Método para deletar um usuário

    static async deleteUser(req, res) {
        const { idUsuario } = req.params;

        try {
            // Chama o serviço e verifica se o usuário existe antes de deletar
            const user = await userService.getById(idUsuario);

            if (!user) {
                return res.status(404).json({ message: 'Usuário não encontrado' });
            } // Retorna um erro caso o usuário não seja encontrado

            
            const deleteQuery = await db.query(
                'DELETE FROM usuario WHERE idUsuario = ?',
                [idUsuario]
            ); // Deleta o usuário do banco de dados

            return res.status(200).json({ message: 'Usuário deletado com sucesso' });
        } catch (e) {
            console.error(e); // Log de erro para depuração
            return res.status(500).json({ error: 'Erro ao deletar o usuário' });
        }
    }
}

module.exports = UserController;
