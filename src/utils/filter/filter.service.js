// Função para remover objetos vazios de um array
function removerObjetosVazios(resposta) {
  if (Array.isArray(resposta)) {
      return resposta.filter(item => {
          if (item && typeof item === "object" && Object.keys(item).length === 0) {
              return false; // Filtra objetos vazios
          }
          return true;
      });
  }
  return resposta;
}

// Função para filtrar propriedades irrelevantes da resposta do banco de dados
function filtrarResposta(resposta) {
  // Verificar se a resposta é um objeto
  if (!resposta || typeof resposta !== "object") {
      return resposta;
  }

  // Lista de propriedades a serem removidas da resposta
  const propriedadesParaRemover = [
      // Propriedades específicas do objeto de resposta que são irrelevantes
      "_buf",
      "_clientEncoding",
      "_catalogLength",
      "_catalogStart",
      "_schemaLength",
      "_schemaStart",
      "_tableLength",
      "_tableStart",
      "_orgTableLength",
      "_orgTableStart",
      "_orgNameLength",
      "_orgNameStart",
      "characterSet",
      "encoding",
      "name",
      "columnLength",
      "columnType",
      "type",
      "flags",
      "decimals"
  ];

  // Iterar sobre cada propriedade a ser removida
  propriedadesParaRemover.forEach((propriedade) => {
      // Verificar se a resposta é um array
      if (Array.isArray(resposta)) {
          // Se for um array, remover as propriedades de cada item
          resposta = resposta.map((item) => {
              delete item[propriedade];
              return filtrarResposta(item);
          });
      } else {
          // Se for um objeto, remover as propriedades diretamente
          delete resposta[propriedade];
      }
  });

  // Remover objetos vazios da resposta
  resposta = removerObjetosVazios(resposta);

  // Retornar a resposta filtrada
  return resposta;
}

module.exports = filtrarResposta;
