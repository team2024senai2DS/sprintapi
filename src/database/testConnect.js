const db = require('./connect');

function testConnect(res) {
    try {
        const test = db.query(
            'SELECT "Conexão bem-sucedida" AS Mensagem'
        );

        return res.status(200).json({ message: 'Conexão Realizada com Mysql! 🗃️', test });
    } catch (error) {
        console.error(error);
        return res.status(500).json({ error: 'Erro ao executar a consulta:', error });
    }
}

module.exports = testConnect;
