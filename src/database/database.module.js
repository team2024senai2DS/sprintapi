const router = require('express').Router();
const databaseController = require('./database.controller')

router.get("/getTables", databaseController.getTables)
router.get("/descUsuario", databaseController.descUsuario)
router.get("/descLivro", databaseController.descLivro)
router.get("/descReserva", databaseController.descReserva)


module.exports = router