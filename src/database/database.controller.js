const db = require("./connect");
const filtrarResposta = require('../utils/filter/filter.service');


// Classe para controlar operações relacionadas ao banco de dados
class dbController {
  // Método para obter os nomes das tabelas do banco de dados
  static async getTables(req, res) {
    try {
      // Executar a consulta SQL para obter os nomes das tabelas
      const tables = await db.query("SHOW TABLES");

      // Extrair apenas os nomes das tabelas e filtrar a resposta
      const tableNames = filtrarResposta(tables);

      // Enviar os nomes das tabelas na resposta HTTP
      return res
        .status(200)
        .json({ message: "Estas são as tabelas:", tableNames });
    } catch (error) {
      // Lidar com erros caso a consulta SQL falhe
      console.error(error);
      return res
        .status(500)
        .json({
          error: "Não foi possível conectar à base de dados SQL.",
          error,
        });
    }
  }

  // Métodos para descrever a estrutura das tabelas do banco de dados
  static async descUsuario(req, res) {
    try {
      // Executar a consulta SQL para descrever a tabela 'usuario'
      const usuario = await db.query("desc usuario");

      // Filtrar a resposta para remover propriedades irrelevantes
      const respostaFiltrada = filtrarResposta(usuario);

      // Enviar a resposta filtrada na resposta HTTP
      return res.status(200).json({ message: "Resultado", respostaFiltrada });
    } catch (error) {
      // Lidar com erros caso a consulta SQL falhe
      console.error(error);
      return res
        .status(500)
        .json({
          error: "Não foi possível conectar à base de dados SQL.",
          error,
        });
    }
  }

  // Método semelhante para descrever a tabela 'endereco'
  static async descEndereco(req, res) {
    try {
      const endereco = await db.query("desc endereco");
      const respostaFiltrada = filtrarResposta(endereco);
      return res.status(200).json({ message: "Resultado", respostaFiltrada });
    } catch (error) {
      console.error(error);
      return res
        .status(500)
        .json({
          error: "Não foi possível conectar à base de dados SQL.",
          error,
        });
    }
  }

  // Método semelhante para descrever a tabela 'livro'
  static async descLivro(req, res) {
    try {
      const livro = await db.query("desc Livro");
      const respostaFiltrada = filtrarResposta(livro);
      return res.status(200).json({ message: "Resultado", respostaFiltrada });
    } catch (error) {
      console.error(error);
      return res
        .status(500)
        .json({
          error: "Não foi possível conectar à base de dados SQL.",
          error,
        });
    }
  }

  // Método semelhante para descrever a tabela 'reserva'
  static async descReserva(req, res) {
    try {
      const reserva = await db.query("desc Reserva");
      const respostaFiltrada = filtrarResposta(reserva);
      return res.status(200).json({ message: "Resultado", respostaFiltrada });
    } catch (error) {
      console.error(error);
      return res
        .status(500)
        .json({
          error: "Não foi possível conectar à base de dados SQL.",
          error,
        });
    }
  }
}

// Exportar a classe dbController para ser usada em outros módulos
module.exports = dbController;
