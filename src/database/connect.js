require('dotenv').config(); // Carrega as variáveis de ambiente do arquivo .env
const mysql = require('mysql2/promise');

try {
    const pool = mysql.createPool({
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_DATABASE,
    });

    console.log('Conectado ao MySQL com sucesso! 🗃️');

    module.exports = pool;
} catch (error) {
    console.error('Erro ao conectar ao banco de dados:', error);
}
