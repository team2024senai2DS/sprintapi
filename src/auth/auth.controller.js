const authService = require('./auth.service');

class authController {

    // Método para autenticar um usuário
    static async signIn(req, res) {
        const { email, password } = req.body;

        try {
            // Verifica se o email e a senha foram fornecidos
            if (!email || !password) {
                return res.status(400).json({ message: 'Email e senha devem ser fornecidos' });
            }
            // Autentica o usuário e retorna o token de acesso e informações do usuário
            const { token, user } = await authService.authenticateUser(email, password);

            return res.status(200).json({ token, user });
        } catch (error) {
            // Lida com erros específicos de autenticação
            if (error.message === 'Usuário não encontrado' || error.message === 'Credenciais inválidas') {
                return res.status(401).json({ message: error.message });
            }
            console.error(error); // Log de outros erros para depuração
            return res.status(500).json({ error: "Erro ao fazer login." });
        }
    }
}

module.exports = authController;