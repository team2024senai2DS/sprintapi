const db = require('../database/connect');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const authenticateUser = async (email, password) => {
    try {
        // Normaliza o email para garantir a correspondência insensível a maiúsculas e minúsculas
        const normalizedEmail = email.toLowerCase();

        // Consulta o usuário pelo email na base de dados
        const [rows] = await db.query('SELECT * FROM usuario WHERE LOWER(email) = ?', [normalizedEmail]);
        const user = rows[0]; // Obtenha o primeiro resultado (se houver)

        if (!user) {
            throw new Error('Usuário não encontrado');
        }
        // Verifica se a senha corresponde usando bcrypt.compare, retorna true se corresponder
        const passwordMatch = await bcrypt.compare(password, user.password);
        if (!passwordMatch) {
            throw new Error('Credenciais inválidas');
        }
        // Gera o token JWT com informações do usuário e define o tempo de expiração para 1 hora
        const token = jwt.sign({ id: user.id, email: user.email }, 'your_secret_key', { expiresIn: '1h' });

        return { token, user };
    } catch (error) {
        throw error; // Propaga o erro para tratamento no controller
    }
};

module.exports = {
    authenticateUser,
};
