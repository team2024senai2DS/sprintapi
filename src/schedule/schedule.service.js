const filtrarResposta = require('../utils/filter/filter.service');
const db = require('../database/connect');

// Verifica a validade dos dados da reserva e se há conflitos de horários
const verifySchedule = async (req, res) => {
    const { idUsuario, idLivro, dataReserva, dataExpiracao } = req.body;

    // Verifica se todos os campos obrigatórios estão presentes
    if (!idUsuario || !idLivro || !dataReserva || !dataExpiracao) {
        return res.status(400).json({ error: 'Todos os campos são obrigatórios.' });
    }

    try {
        // Consulta para verificar conflitos de horário com outras reservas
        const query = await db.query(
            'SELECT * FROM reserva WHERE idLivro = ? AND ((? <= dataExpiracao AND ? >= dataReserva) OR (? <= dataExpiracao AND ? >= dataReserva))',
            [idLivro, dataReserva, dataReserva, dataExpiracao, dataExpiracao]
        );

        if (query[0].length > 0) { // Corrigido: Acessando o resultado da query
            return res.status(400).json({ error: "Livro já reservado nesse período!" });
        } else {
            return { status: 200, message: 'Reserva válida' }; // Retorno de sucesso para o controller
        }
    } catch (error) {
        console.error(error); // Log de erro para depuração
        return res.status(500).json({ error: 'Erro interno do servidor.' });
    }
};

// Obtém todas as reservas do banco de dados
const getSchedules = async () => {
    const query = await db.query(
        'SELECT * FROM Reserva'
    );

    const queryFiltered = filtrarResposta(query); // Filtra a resposta usando utilitário de filtro
    try {
        return queryFiltered; 
    } catch (error) {
        console.error(error); // Log de erro para depuração
    }
}

// Obtém uma reserva específica pelo ID
const getById = async (idReserva) => {
    const query = await db.query(
        'SELECT * FROM Reserva WHERE idReserva = ?',
        [idReserva]
    );

    const queryFiltered = filtrarResposta(query); // Filtra a resposta usando utilitário de filtro

    try {
        return queryFiltered.length > 0 ? queryFiltered[0] : null; // Retorna a reserva ou null se não encontrada

    } catch (error) {
        console.error(error); // Log de erro para depuração
        throw new error('Reserva não encontrada');
    }
}

// Deleta um livro associado a uma reserva específica
const deleteBook = async (req, res) => {
    const idReserva = req.params.idReserva;

    const bookQuery = await db.query(
        'DELETE FROM livro WHERE idReserva = ?',
        [idReserva]
    );

    try {
        if (bookQuery.affectedRows === 0) { // Acessa o resultado da query
            return res.status(400).json({ error: "Livro não existe dentro da reserva!" });
        } else {
            return { status: 200, message: 'Livro deletado com sucesso!' }; // Retorno de sucesso para o controller
        }

    } catch (error) {
        console.error(error); // Log de erro para depuração
        return res.status(500).json({ error: "Erro ao deletar o livro" });
    }
}

const getReservedBooksByUser = async (idUsuario) => {
    // Busca todos os livros reservados por um usuário específico
    const query = await db.query(
        'SELECT livro.* FROM livro JOIN reserva ON livro.idLivro = reserva.idLivro WHERE reserva.idUsuario = ?',
        [idUsuario]
    );

    // Filtra a resposta 
    const queryFiltered = filtrarResposta(query);
    try {
        return queryFiltered; // Retorna a lista de livros reservados pelo usuário
    } catch (error) {
        console.error(error); // Log de erro para depuração
    }
}

module.exports = {
    verifySchedule,
    getSchedules,
    getById,
    deleteBook,
    getReservedBooksByUser,
};
