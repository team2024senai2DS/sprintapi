const scheduleService = require('./schedule.service');
const db = require('../database/connect');

class scheduleController {

    // Método para cadastrar uma nova reserva
    static async createSchedule(req, res) {
        const { idUsuario, idLivro, dataReserva, dataExpiracao } = req.body;    
    
        // Chama o serviço e verifica a validade dos dados da reserva
        const scheduleVerification = await scheduleService.verifySchedule(req, res);
        if (scheduleVerification.status !== 200) {
            return res.status(scheduleVerification.status).json(scheduleVerification);
        }
    
        try {
            // Verifica se já existe uma reserva para o mesmo usuário e livro
            const existingReservation = await db.query(
                'SELECT * FROM reserva WHERE idUsuario = ? AND idLivro = ?',
                [idUsuario, idLivro]
            );
    
            if (existingReservation[0].length > 0) {
                return res.status(400).json({ error: "Este usuário já possui uma reserva para este livro." });
            }
        } catch (error) {
            console.error("Erro ao verificar reserva existente:", error); // Log de erro para depuração
            return res.status(500).json({ error: "Erro interno do servidor" });
        }
    
        try {
            // Insere uma nova reserva no banco de dados
            const insertQuery = await db.query(
                'INSERT INTO reserva (idUsuario, idLivro, dataReserva, dataExpiracao) VALUES (?, ?, ?, ?)',
                [idUsuario, idLivro, dataReserva, dataExpiracao]
            );
    
            // Obter o idReserva recém-inserido
             const idReserva = insertQuery[0].insertId
    
            // Atualiza a tabela livro com o novo idReserva e muda sua disponibilidade para Reservado
            await db.query(
                `UPDATE livro SET idReserva = ?, disponibilidade = 'Reservado' WHERE idLivro = ?`,
                [idReserva, idLivro]
            );
    
            return res.status(201).json({
                message: "Reserva cadastrada com sucesso",
                scheduleId: idReserva // Acessa o ID da reserva inserida
            });
        } catch (error) {
            console.error("Erro ao inserir reserva:", error); // Log de erro para depuração 
    
            if (error.code === 'ER_DUP_ENTRY') {
                return res.status(400).json({ error: "Este usuário já possui uma reserva para este livro." });
            } else {
                return res.status(500).json({ error: "Erro interno do servidor" });
            }
        }
    }
    

    // Método para buscar todas as reservas
    static async getAllSchedules(req, res) {
        // Chama o serviço que busca todas as reservas
        const schedules = await scheduleService.getSchedules();

        try {
            return res.status(200).json({
                message: "Estas são as reservas cadastradas:", schedules
            }); // Retorna uma lista que contém todas as reservas cadastradas
        } catch (error) {
            console.error(error); // Log de erro para depuração
            return res.status(500).json({
                error: "Não foi possível conectar à base de dados SQL",
                details: error.message,
            });
        }
    }

    // Método para buscar uma reserva pelo ID
    static async getScheduleById(req, res) {
        const idReserva = req.params.idReserva;

        try {
            // Chama o serviço que busca a reserva pelo ID via parametros
            const schedule = await scheduleService.getById(idReserva);

            if (!schedule) {
                return res.status(404).json({ message: 'Reserva não encontrada' });
            }

            return res.status(200).json({
                message: `Este é o livro cadastrado do id: ${idReserva}`,
                schedule,
            });
        } catch (error) {
            console.error(error); // Log de erro para depuração
            return res.status(500).json({
                error: "Erro ao buscar reserva",
                details: error.message,
            });
        }
    }

    // Método para atualizar os dados de uma reserva
    static async updateSchedule(req, res) {
        const { idUsuario, idLivro, dataReserva, dataExpiracao } = req.body;
        const { idReserva } = req.params;

        try {
            // Verifica quais campos estão presentes na requisição para atualizar
            const fields = {};
            if (idUsuario) fields.idUsuario = idUsuario;
            if (idLivro) fields.idLivro = idLivro;
            if (dataReserva) fields.dataReserva = dataReserva;
            if (dataExpiracao) fields.dataExpiracao = dataExpiracao;

            if (Object.keys(fields).length === 0) {
                return res.status(400).json({ message: 'Nenhum campo para atualizar' });
            }

            // Constrói a query dinamicamente com os campos a serem atualizados
            const setClause = Object.keys(fields)
                .map(field => `${field} = ?`)
                .join(', ');
            const values = Object.values(fields);

            // Adiciona o idReserva ao final dos valores para a query
            values.push(idReserva);

            const query = await db.query(
                `UPDATE reserva SET ${setClause} WHERE idReserva = ?`,
                values
            );

            return res.status(200).json({
                message: "Reserva atualizado com sucesso",
                userId: idUsuario
            });
        } catch (e) {
            console.error(e); // Log de erro para depuração
            return res.status(500).json({ error: "Erro ao atualizar reserva" });
        }
    }

    // Método para deletar uma reserva
    static async deleteSchedule(req, res) {
        const idReserva = req.params.idReserva;

        try {
            // Atualiza o livro associado à reserva para disponível e remove a referência da reserva
            const updateBookQuery = await db.query(
                'UPDATE Livro SET idReserva = NULL, disponibilidade = "Disponível" WHERE idReserva = ?',
                [idReserva]
            );

            // Deleta a reserva do banco de dados
            const deleteReservaQuery = await db.query(
                'DELETE FROM Reserva WHERE idReserva = ?',
                [idReserva]
            );

            return res.status(200).json({ message: 'Reserva e livro associado deletados com sucesso' });

        } catch (e) {
            console.error(e); // Log de erro para depuração
            return res.status(500).json({ error: "Erro ao deletar a reserva e o livro associado" });
        }
    }

    // Método para buscar livros reservados por um usuário específico
    static async getReservedBooksByUser(req, res) {
        const idUsuario = req.params.idUsuario;

        try {
            // Chama o serviço que busca os livros reservados pelo usuário
            const reservedBooks = await scheduleService.getReservedBooksByUser(idUsuario);

            if (!reservedBooks || reservedBooks.length === 0) {
                return res.status(404).json({ message: 'Nenhum livro reservado encontrado para este usuário' });
            }

            return res.status(200).json({
                message: `Livros reservados pelo usuário ${idUsuario}:`,
                reservedBooks,
            });
        } catch (error) {
            console.error(error); // Log de erro para depuração
            return res.status(500).json({
                error: "Erro ao buscar os livros reservados",
                details: error.message,
            });
        }
    }

}

module.exports = scheduleController;