const router = require('express').Router();
const scheduleController = require('./schedule.controller')

router.post('/createSchedule', scheduleController.createSchedule);
router.get('/getAllSchedules', scheduleController.getAllSchedules);
router.get('/getScheduleById/:idReserva', scheduleController.getScheduleById);
router.get('/getReservedBooksByUser/:idUsuario', scheduleController.getReservedBooksByUser);
router.put('/updateSchedule/:idReserva', scheduleController.updateSchedule);
router.delete('/deleteSchedule/:idReserva', scheduleController.deleteSchedule);



module.exports = router