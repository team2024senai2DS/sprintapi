const router = require('express').Router();
const bookController = require('./book.controller')

router.post('/createBook', bookController.createBook);
router.get('/getAllBooks', bookController.getAllBooks);
router.get('/getBooksAvailability/', bookController.getBookAvalability);
router.get('/getBookById/:idLivro', bookController.getBookById);
router.put('/updateBook/:idLivro', bookController.updateBook);
router.delete('/deleteBook/:idLivro', bookController.deleteBook);




module.exports = router;