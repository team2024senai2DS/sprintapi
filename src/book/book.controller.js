const bookService = require('./book.service');
const db = require('../database/connect');

class bookController {

    // Método para cadastrar um novo livro
    static async createBook(req, res) {
        const { titulo, autores, editora, dataDePublicacao, genero, imageUrl, disponibilidade } = req.body;

        try {
            // Chama o serviço e verifica a validade dos dados do livro
            const validationResult = await bookService.verifyBook(req, res);

            if (validationResult.status === 200) {
                // Insere o livro no banco de dados
                const query = await db.query(
                    'INSERT INTO Livro (titulo, autor, editora, dataDePublicacao, genero, imageUrl, disponibilidade) VALUES (?, ?, ?, ?, ?, ?, ?)',
                    [titulo, autores, editora, dataDePublicacao, genero, imageUrl, disponibilidade]
                );
                return res.status(201).json({
                    message: 'Livro cadastrado com sucesso',
                    bookId: query.insertId // Inclui o ID do livro cadastrado
                });
            } else {
                return res.status(validationResult.status).json({
                    error: validationResult.message
                }) // Retorna as respostas das validações
            }
        } catch (error) {
            console.error(error); // Log do erro para depuração
            return res.status(500).json({ error: 'Erro interno do servidor' });
        }
    }

    // Método para buscar todos os livros
    static async getAllBooks(req, res) {
        // Chama o serviço que busca todos os livros
        const books = await bookService.getBooks();

        try {
            return res.status(200).json({
                message: "Estes são os livros cadastrados:", books
            });
        } catch (error) {
            console.error(error); // Log de erro para depuração
            return res.status(500).json({
                error: "Não foi possível conectar à base de dados SQL",
                details: error.message,
            });
        }
    }

    // Método para buscar livros disponíveis
    static async getBookAvalability(req, res) {
        // Chama o serviço que busca todos os livros pela disponibilidade
        const getBooks = await bookService.getBooksByAvailability();

        try {
            return res.status(200).json({
                message: "Estes são os livros disponiveis:", getBooks
            });
        } catch (error) {
            console.error(error);
            return res.status(500).json({
                error: "Não foi possível conectar à base de dados SQL",
                details: error.message,
            });
        }
    }

    // Método para buscar um livro pelo ID
    static async getBookById(req, res) {
        const idLivro = req.params.idLivro;

        try {
            // Chama o serviço que busca o livro pelo ID via parametros
            const book = await bookService.getById(idLivro);

            if (!book) {
                return res.status(404).json({ message: 'Livro não encontrado' });
            }

            return res.status(200).json({
                message: `Este é o livro cadastrado do id: ${idLivro}`,
                book,
            });
        } catch (error) {
            console.error(error); // Log de erro para depuração
            return res.status(500).json({
                error: "Erro ao buscar o livro",
                details: error.message,
            });
        }
    }

    // Método para atualizar os dados de um livro
    static async updateBook(req, res) {
        const { titulo, autores, editora, dataDePublicacao, genero, imageUrl, disponibilidade } = req.body;
        const { idLivro } = req.params;

        try {
            // Verifica quais campos estão presentes na requisição para atualizar
            const fields = {};
            if (titulo) fields.titulo = titulo;
            if (autores) fields.autores = autores;
            if (editora) fields.editora = editora;
            if (dataDePublicacao) fields.dataDePublicacao = dataDePublicacao;
            if (genero) fields.genero = genero;
            if (imageUrl) fields.imageUrl = imageUrl;
            if (disponibilidade) fields.disponibilidade = disponibilidade

            if (Object.keys(fields).length === 0) {
                return res.status(400).json({ message: 'Nenhum campo para atualizar' });
            }

            // Constrói a query dinamicamente com os campos a serem atualizados
            const setClause = Object.keys(fields)
                .map(field => `${field} = ?`)
                .join(', ');
            const values = Object.values(fields);

            // Adiciona o idLivro ao final dos valores para a query
            values.push(idLivro);

            const query = await db.query(
                `UPDATE livro SET ${setClause} WHERE idLivro = ?`,
                values
            );

            return res.status(200).json({
                message: "Livro atualizado com sucesso",
                userId: idLivro
            });
        } catch (e) {
            console.error(e); // Log de erro para depuração
            return res.status(500).json({ error: "Erro ao atualizar o livro" });
        }
    }

    // Método para deletar um livro
    static async deleteBook(req, res) {
        const { idLivro } = req.params;

        try {
            // Chama o serviço e busca o livro via ID
            const book = await bookService.getById(idLivro);

            if (!book) {
                return res.status(404).json({ message: 'Livro não encontrado' });
            }

            // Deleta o livro do banco de dados
            const deleteQuery = await db.query(
                'DELETE FROM livro WHERE idLivro = ?',
                [idLivro]
            );

            return res.status(200).json({ message: 'Livro deletado com sucesso' });
        } catch (e) {
            console.error(e); // Log de erro para depuração
            return res.status(500).json({ error: 'Erro ao deletar o livro' });
        }
    }

}

module.exports = bookController;