const db = require('../database/connect');
const filtrarResposta = require('../utils/filter/filter.service');

// Verifica a validade dos dados do livro
const verifyBook = async (req, res) => {
    const { titulo, autores, editora, dataDePublicacao, genero, imageUrl, disponibilidade } = req.body;

    try {
        // Verificar se todos os campos obrigatórios estão preenchidos
        if (!titulo || !autores || !editora || !imageUrl) {
            return res.status(400).json({ message: 'Título, autores, editora e imagem são obrigatórios' });
        }

        // Verificar se autores é um array e se contém pelo menos um autor
        if (!Array.isArray(autores) || autores.length === 0) {
            return res.status(400).json({ message: 'Autores deve ser um array com pelo menos um autor' });
        }

        // Verificar se todos os elementos do array autores são strings válidas
        if (!autores.every(autor => typeof autor === 'string' && autor.trim() !== '')) {
            return res.status(400).json({ message: 'Todos os autores devem ser strings válidas' });
        }

        // Verificar se a data de publicação é válida (opcional, adicione se necessário)
        if (dataDePublicacao) {
            const dataRegex = /^\d{4}-\d{2}-\d{2}$/; // Formato YYYY-MM-DD
            if (!dataRegex.test(dataDePublicacao)) {
                return res.status(400).json({ message: 'Data de publicação inválida' });
            }
        }

        const disponibilidade = req.body.disponibilidade.toLowerCase() === 'disponível';
        // Converte 'disponibilidade' para booleano e verifica se é válido
        if (typeof disponibilidade !== 'boolean') {
            return res.status(400).json({ message: 'Valor de disponibilidade inválido. Use "Disponível" ou "Reservado".' });
        }

        // Se todas as verificações passarem, o livro é considerado válido
        return { status: 200, message: 'Livro válido' };
    } catch (error) {
        console.error(error); // Log de erro para depuração
        return res.status(500).json({ message: 'Erro no servidor' });
    }
};

// Obtém todos os livros do banco de dados
const getBooks = async () => {
    // Busca todos os livros no banco de dados
    const query = await db.query(
        'SELECT * FROM Livro'
    );
    // Filtra a resposta
    const queryFiltered = filtrarResposta(query);
    try {
        return queryFiltered; // Retorna a lista de livros
    } catch (error) {
        console.error(error); // Log de erro para depuração
    }
}

const getBooksByAvailability = async () => {
    // Busca todos os livros disponíveis no banco de dados
    const query = await db.query(
        'SELECT * FROM Livro WHERE disponibilidade = ?',
        ['disponível']
    );

    // Filtra a resposta 
    const queryFiltered = filtrarResposta(query);
    try {
        return queryFiltered; // Retorna a lista de livros disponíveis
    } catch (error) {
        console.error(error); // Log de erro para depuração
    }
}

const getById = async (idLivro) => {
    // Busca um livro específico pelo ID no banco de dados
    const query = await db.query(
        'SELECT * FROM Livro WHERE idLivro = ?',
        [idLivro]
    );

    // Filtra a resposta 
    const queryFiltered = filtrarResposta(query);

    try {
        return queryFiltered.length > 0 ? queryFiltered[0] : null; // Retorna o livro se encontrado

    } catch (error) {
        console.error(error); // Log de erro para depuração
        throw new error('Livro não encontrado'); // Lança um erro
    }
}

module.exports = {
    verifyBook,
    getBooks,
    getById,
    getBooksByAvailability,
};
