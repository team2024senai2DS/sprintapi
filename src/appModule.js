const express = require('express');
const router = express.Router();

const databaseModule = require('./database/database.module');
const userModule = require('./user/user.module');
const authModule = require('./auth/auth.module');
const bookModule = require('./book/book.module');
const scheduleModule = require('./schedule/schedule.module');


router.use('/database', databaseModule);
router.use('/user', userModule);
router.use('/auth', authModule);
router.use('/book', bookModule);
router.use('/schedule', scheduleModule);

module.exports = router;
