# Sprint API Books



> Esta API RESTful, está sendo desenvolvida para estudo de JavaScript, implementa as operações CRUD (Criar, Ler, Atualizar e Deletar) para gerenciar reservas de livros em bibliotecas. Para otimizar a organização e escalabilidade do projeto, foi adotado o padrão MVC (Model-View-Controller), inspirado no framework NestJS, proporcionando uma estrutura modular e eficiente para o desenvolvimento da API.
A utilização do padrão MVC, aliado à inspiração no NestJS, proporciona um código mais limpo, organizado e de fácil manutenção, facilitando o desenvolvimento e a evolução da API.

### Ajustes e melhorias

O projeto ainda está em desenvolvimento e as próximas atualizações serão voltadas nas seguintes tarefas:

- [x] Criar o app usando Express
- [x] Criar o CRUD de usuários - Em progresso: 50% 
- [x] Criar o CRUD de livros - Em progresso: 50% 
- [x] Criar o CRUD de reservas - Em progresso: 50% 
- [x] Criar as rotas HTTP
- [x] Criar lógica para efetuar login usando JWT
- [x] Efetuar a conexão com o banco de dados (MySQL)
- [x] Criar rotas para testar a conexão com o banco
- [x] Refatorar as controllers para que possam receber as querys do MySQL
- [x] Criar função para buscar usuários pelo ID
- [x] Refatorar código para melhor execução
- [x] Refatorar o projeto para o padrão MVC

## 💻 Pré-requisitos

* Node.js

**Passos:**

1. **Instalar o Node.js:** Acesse o site oficial <https://nodejs.org/en> e baixe a versão mais recente para o seu sistema operacional.


## 🚀 Instalando Sprint API

Para instalar o Sprint API, siga estas etapas:

1. **Clonar o projeto:** Utilize o comando abaixo para clonar o projeto para o seu computador.

```bash
git clone https://gitlab.com/projetossenai2024/apisenai/sprintapi.git
```

2. **Instalar as dependências:** Navegue até a pasta do projeto e execute o comando:

```bash
npm install
```

Com isto as dependências serão instaladas.

## ☕ Usando Sprint API

Para usar a Sprint API, siga estas etapas:

**Iniciar a API:**

1. Execute o comando `npm run start:dev` no terminal.
2. A API estará disponível na porta `5000`.

**Consumir a API no Postman:**

1. Abra o Postman e crie uma nova requisição.
2. Importe a coleção disponível neste repositório: [Coleção](postmanAPI.collection.json).
3. Escolha o método HTTP desejado (POST, GET, PUT ou DELETE) que você deseja testar.
4. Preencha os campos com os dados que você achar melhor
5. Clique em "Enviar" para executar a requisição.


A coleção disponível contém exemplos completos de diferentes métodos HTTP para testar a API.

## 📫 Contribuindo para o projeto

Contribuições são sempre bem-vindas!

Veja o [CONTRIBUINDO](CONTRIBUTING.md) para saber como começar.


## 🤝 Colaboradores

<table>
  <tr>
    <td align="center">
      <a href="https://github.com/igordmouraa">
        <img src="https://avatars.githubusercontent.com/u/127807075" width="100px;" alt="Foto do Igor Moura no GitHub"/><br>
        <sub>
          <b>Igor Moura</b>
        </sub>
      </a>
    </td>
        <td align="center">
      <a href="https://gitlab.com/gabrielazevedo1222">
        <img src="https://gitlab.com/uploads/-/system/user/avatar/13628288/avatar.png?width=800" width="100px;" alt="Foto do Gabriel Azevedo no GitLab"/><br>
        <sub>
          <b>Gabriel Azevedo Gonçalves</b>
        </sub>
      </a>
    </td>
        <td align="center">
      <a href="https://gitlab.com/vinizamara">
        <img src="https://secure.gravatar.com/avatar/a043cc90e5fdf5c1eccb44b99bd596ebc7c4ca2fa547aef9bea7389c5031d629?s=1600&d=identicon" width="100px;" alt="Foto do Vinicius Manfrin no Gitlab"/><br>
        <sub>
          <b>Vinicius Manfrin Zamara</b>
        </sub>
      </a>
    </td>
    
  </tr>
</table>

## 📝 Licença

Esse projeto está sob licença. Veja o arquivo [LICENÇA](LICENSE) para mais detalhes.